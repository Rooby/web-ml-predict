<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MLPredict extends Command
{
    protected $signature = 'model:predict';

    protected $description = 'Test if the ML model is working';

    protected $mapActivities = [
        "0" => "SITTING",
        "1" => "STANDING",
        "2" => "WALKING"
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $modelPath = public_path() . "/model/RNN_Custom.h5";
        $csvFilePath = public_path() . "/model/predict.csv";
        $json = system("python " . public_path() . "/model/predict.py " . $modelPath . " " . $csvFilePath);
        $result = json_decode($json, true);
        $finalActivityClass = $result[count($result) - 1];
        echo "You are " . $this->mapActivities[$finalActivityClass];
    }
}