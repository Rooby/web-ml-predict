<?php

namespace App;

class MLModel
{
    public static function predict($data)
    {
        self::writeData($data);
        $modelPath = public_path() . "/model/RNN_Custom.h5";
        $csvFilePath = public_path() . "/model/";
        $json = exec("python " . public_path() . "/model/predict.py " . $modelPath . " " . $csvFilePath);
        $result = json_decode($json, true);

        $finalActivityClass = $result[count($result) - 1];
        return $finalActivityClass;
    }

    public static function writeData($data)
    {
        $accData = json_decode($data["accData"]);
        $gyroData = json_decode($data["gyroData"]);

        $xAccFilePath = public_path() . "/model/x_acc.csv";
        $yAccFilePath = public_path() . "/model/y_acc.csv";
        $zAccFilePath = public_path() . "/model/z_acc.csv";

        $xGyroFilePath = public_path() . "/model/x_gyro.csv";
        $yGyroFilePath = public_path() . "/model/y_gyro.csv";
        $zGyroFilePath = public_path() . "/model/z_gyro.csv";
        
        $xAccFile = fopen($xAccFilePath, 'w');
        $yAccFile = fopen($yAccFilePath, 'w');
        $zAccFile = fopen($zAccFilePath, 'w');

        $xGyroFile = fopen($xGyroFilePath, 'w');
        $yGyroFile = fopen($yGyroFilePath, 'w');
        $zGyroFile = fopen($zGyroFilePath, 'w');

        $xAcc = [];
        $yAcc = [];
        $zAcc = [];

        $xGyro = [];
        $yGyro = [];
        $zGyro = [];

        foreach ($accData as $key => $value) {
            if ($key % 3 == 0) {
                $xAcc[] = $accData[$key + 0];
                $yAcc[] = $accData[$key + 1];
                $zAcc[] = $accData[$key + 2];
                $xGyro[] = $gyroData[$key + 0];
                $yGyro[] = $gyroData[$key + 1];
                $zGyro[] = $gyroData[$key + 2];
            }
        }

        fputcsv($xAccFile, $xAcc);
        fputcsv($yAccFile, $yAcc);
        fputcsv($zAccFile, $zAcc);

        fputcsv($xGyroFile, $xGyro);
        fputcsv($yGyroFile, $yGyro);
        fputcsv($zGyroFile, $zGyro);

        fclose($xAccFile);
        fclose($yAccFile);
        fclose($zAccFile);

        fclose($xGyroFile);
        fclose($yGyroFile);
        fclose($zGyroFile);
    }
}
