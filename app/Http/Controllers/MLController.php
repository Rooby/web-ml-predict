<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MLModel;

class MLController extends Controller
{
    protected $mapActivities = [
        "0" => "SITTING",
        "1" => "WALKING",
        "2" => "STANDING",
    ];

    public function predict(Request $request)
    {
        $finalActivityClass = MLModel::predict($request->all());
        $response = [
            "message"   => "You are " . $this->mapActivities[$finalActivityClass]
        ];
        return response($response, 200); 
    }
}