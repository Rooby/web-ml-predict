import tensorflow as tf
import json
import sys
import numpy as np
from pandas import read_csv
from numpy import dstack
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

# load a single file as a numpy array
def load_file(filepath):
    dataframe = read_csv(filepath, header=None, sep=",")
    return dataframe.values


# load a list of files and return as a 3d numpy array
def load_group(filenames, prefix=''):
    loaded = list()
    for name in filenames:
        data = load_file(prefix + name)
        loaded.append(data)
    # stack group so that features are the 3rd dimension
    loaded = dstack(loaded)
    return loaded


# load a dataset group, such as train or test
def load_dataset_group():
    filepath = sys.argv[2]

    filenames = list()
    filenames += ['x_acc.csv', 'y_acc.csv', 'z_acc.csv']
    filenames += ['x_gyro.csv', 'y_gyro.csv', 'z_gyro.csv']

    x = load_group(filenames, filepath)

    return x


def run():
	model = tf.keras.models.load_model(sys.argv[1])
	x = load_dataset_group()

	predict = model.predict_classes(x, 64)
	jsonData = json.dumps(predict.tolist())
	print(jsonData)

run()
